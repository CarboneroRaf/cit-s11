package com.zuitt.activity.repositories;

import org.springframework.stereotype.Repository;
import org.springframework.data.repository.CrudRepository;
import com.zuitt.activity.models.User;

@Repository
public interface UserRepository extends CrudRepository<User, Object>{



}
